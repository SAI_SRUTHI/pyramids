import sys

def base_value(n):
    i=2
    while(HighPy(i)<=n):
        i=i+1
    return i-1

def HighPy(num):
    return int((num * (num + 1) * (2*num + 1))/6)


def LowPy(num):
    if num > 2:
        return int(( num * (num + 2) * (num + 1))/6)
    return -1



def construction(b,j):
    if HighPy(j) <= b and HighPy(j) > 0:
        m.append(str(j)+"H")
        b -= HighPy(j)
   
    if LowPy(j) <= b and LowPy(j) > 0:
        m.append(str(j)+"L")
        b -= LowPy(j)
    return b


def printPyramid(base,cubes):
    global m
    m = []
    for i in range(base,1,-1):
        cubes = construction(cubes,i)
    if cubes != 0: print("impossible")
    else: print(" ".join(m))
   

for i in range(1,len(sys.argv)):
    cubes = int(sys.argv[i])
    base = base_value(cubes)
    printPyramid(base,cubes)
    i = i + 1


